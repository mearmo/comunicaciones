"""
Librerías y módulos utilizados en el programa
"""
import sys
import random
import numpy as np
import matplotlib.pyplot as plt

"""
- Método PAM_parameters(): 
Toma la densidad de modulación introducida por el usuario y la cantidad de
muestras de muestreo para convertirlas en enteros. También imprime la asignación de bits a símbolos
según la densidad de modulación escogida.
- Returns:
M: Entero con la densidad de modulación
samples: Cantidad de muestras 
"""
def PAM_parameters():
    M = int(sys.argv[2])
    samples = int(sys.argv[3])
    if M == 4:
        print("Ha escogido modulación 4-PAM")
        print("Asignación de bits a símbolos 4-PAM")
        print("              b | a")
        print("             --------")
        print("             00 | -1")
        print("             01 |-1/2")
        print("             10 | 1/2")
        print("             11 |  1")
    elif M == 8:
        print("Ha escogido modulación 8-PAM")
        print("Asignación de bits a símbolos 8-PAM")
        print("              b  |  a")
        print("             --------")
        print("             000 | -1")
        print("             001 |-1/2")
        print("             010 |-1/4")
        print("             011 |-1/8")
        print("             100 | 1/8")
        print("             101 | 1/4")
        print("             110 | 1/2")
        print("             111 |  1") 
    return M, samples

"""
- Método split():
Separa en caracteres un strong dado y los devuelve como una lista
- Parámetros:
word: String a separar en caracteres
- Returns:
list(word): Lista cuyos elementos son los caracteres del string
"""
def split(word): 
    return list(word) 

"""
- Método readSequence():
Método que toma la secuencia de bits ingresada por el usuario y genera una lista con los caracteres
de la secuencia. También revisa que la cantidad de elementos de la secuencia sea múltiplo de la 
cantidad de bits por símbolo según la densidad de modulación. En caso de que no sea así, se agregan
ceros para completar la secuencia
- Parámetros:
M: Densidad de modulación
- Returns:
sequence: Un objeto tipo array de la librería de Numpy con los valores de los bits de la secuencia
separados de forma unitaria
"""
def readSequence(M):
    inputSequence = sys.argv[1]
    lst1 = split(inputSequence)
    lst2 = list()
    for i in range(len(lst1)):
        lst2.append(int(lst1[i]))
    if M == 4:
        if len(lst2) % 2 == 1:
            lst2.append(random.randint(0,1))
    elif M == 8:
        pam_flag = False
        while pam_flag == False:
            if len(lst2) % 3 != 0:
                lst2.append(random.randint(0,1))
            else:
                pam_flag = True
    sequence = np.array(lst2)
    return sequence

"""
- Método modulation_4PAM():
Función que simula la modulación 4-PAM ideal, toma los valores de la secuencia y los agrupa según
la cantidad de bits por símbolos correspondientes (2 bits en este caso). Toma una cantida de muestras
durante cada tiempo de símbolo para modular y grafica el resultado
- Parámetros:
sequence: Objeto de la librería Numpy con los valores de la secuencia separados de forma individual
M: Densidad de modulación
samples: Cantidad de muestras a tomar durannte el tiempo de símbolo
- Returns:
Xt: Lista con los valores modulados que simula la señal X(t)
"""
def modulation_4PAM(sequence, M, samples):
    values = list()
    for i in range(0, len(sequence), 2):
        if sequence[i] == 0 and sequence[i+1] == 0:
            values.append(-1)  
        elif sequence[i] == 0 and sequence[i+1] == 1:
            values.append(-0.5)
        elif sequence[i] == 1 and sequence[i+1] == 0:
            values.append(0.5)
        elif sequence[i] == 1 and sequence[i+1] == 1:
            values.append(1)
    pam_values = np.array(values)
    pt = np.empty(samples)
    pt.fill(1)
    Xt = list()
    Xt_plot = list()
    """
    Este ciclo se encarga de la modulación
    """
    for j in range(len(pam_values)):
        Xi = list()
        for k in range(len(pt)):
            Xt_plot.append(pam_values[j]*pt[k])
            Xi.append(pam_values[j]*pt[k])
        Xt.append(Xi)
    time = np.arange(len(Xt_plot))
    title = "Señal (sin ruido) de modulación 4-PAM para la secuencia de bits: " + sys.argv[1]
    """
    Se grafican los valores modulados
    """
    plt.figure(1)
    plt.plot(time,Xt_plot)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Xt

"""
- Método modulation_4PAM_awgn():
Función que simula la modulación 4-PAM con AWGN, toma los valores de la secuencia y los agrupa según
la cantidad de bits por símbolos correspondientes (2 bits en este caso). Toma una cantida de muestras
durante cada tiempo de símbolo para modular y grafica el resultado
- Parámetros:
sequence: Objeto de la librería Numpy con los valores de la secuencia separados de forma individual
M: Densidad de modulación
samples: Cantidad de muestras a tomar durannte el tiempo de símbolo
- Returns:
Xt_awgn: Lista con los valores modulados con ruido que simula la señal X(t)
"""
def modulation_4PAM_awgn(sequence, M, samples):
    values = list()
    for i in range(0, len(sequence), 2):
        if sequence[i] == 0 and sequence[i+1] == 0:
            values.append(-1)  
        elif sequence[i] == 0 and sequence[i+1] == 1:
            values.append(-0.5)
        elif sequence[i] == 1 and sequence[i+1] == 0:
            values.append(0.5)
        elif sequence[i] == 1 and sequence[i+1] == 1:
            values.append(1)
    pam_values = np.array(values)
    pt = np.empty(samples)
    pt.fill(1)
    Xt_awgn = list()
    Xt_awgn_plot = list()
    """
    Las siguientes líneas crean el ruido y se agrega a los datos de la simulación para simular el canal AWGN
    """
    noise = np.random.normal(0,0.1,samples)
    """
    Este ciclo se encarga de la modulación
    """
    for j in range(len(pam_values)):
        Xi_awgn = list()
        for k in range(len(pt)):
            Xt_awgn_plot.append((pam_values[j]*pt[k]) + noise[k])
            Xi_awgn.append((pam_values[j]*pt[k]) + noise[k])
        Xt_awgn.append(Xi_awgn)
    time = np.arange(len(Xt_awgn_plot))
    title1 = "Señal (con ruido) de modulación 4-PAM para la secuencia de bits: " + sys.argv[1]
    """
    Se grafican los valores modulados
    """
    plt.figure(1)
    plt.plot(time,Xt_awgn_plot)
    plt.title(title1)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Xt_awgn

"""
- Método modulation_8PAM():
Función que simula la modulación 8-PAM ideal, toma los valores de la secuencia y los agrupa según
la cantidad de bits por símbolos correspondientes (3 bits en este caso). Toma una cantida de muestras
durante cada tiempo de símbolo para modular y grafica el resultado
- Parámetros:
sequence: Objeto de la librería Numpy con los valores de la secuencia separados de forma individual
M: Densidad de modulación
samples: Cantidad de muestras a tomar durannte el tiempo de símbolo
- Returns:
Xt: Lista con los valores modulados con ruido que simula la señal X(t)
"""
def modulation_8PAM(sequence, M, samples):
    values = list()
    for i in range(0, len(sequence), 3):
        if sequence[i] == 0 and sequence[i+1] == 0 and sequence[i+2] == 0:
            values.append(-1)  
        elif sequence[i] == 0 and sequence[i+1] == 0 and sequence[i+2] == 1:
            values.append(-0.5)  
        elif sequence[i] == 0 and sequence[i+1] == 1 and sequence[i+2] == 0:
            values.append(-0.25)  
        elif sequence[i] == 0 and sequence[i+1] == 1 and sequence[i+2] == 1:
            values.append(-0.125)  
        elif sequence[i] == 1 and sequence[i+1] == 0 and sequence[i+2] == 0:
            values.append(0.125) 
        elif sequence[i] == 1 and sequence[i+1] == 0 and sequence[i+2] == 1:
            values.append(0.25) 
        elif sequence[i] == 1 and sequence[i+1] == 1 and sequence[i+2] == 0:
            values.append(0.5) 
        elif sequence[i] == 1 and sequence[i+1] == 1 and sequence[i+2] == 1:
            values.append(1)  
    pam_values = np.array(values)
    pt = np.empty(samples)
    pt.fill(1)
    Xt = list()
    Xt_plot = list()
    """
    Este ciclo se encarga de la modulación
    """
    for j in range(len(pam_values)):
        Xi = list()
        for k in range(len(pt)):
            Xt_plot.append(pam_values[j]*pt[k])
            Xi.append(pam_values[j]*pt[k])
        Xt.append(Xi)
    time = np.arange(len(Xt_plot))
    title = "Señal (sin ruido) de modulación 8-PAM para la secuencia de bits: " + sys.argv[1]
    """
    Se grafican los valores modulados
    """
    plt.figure(1)
    plt.plot(time,Xt_plot)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Xt

"""
- Método modulation_8PAM_awgn():
Función que simula la modulación 8-PAM con AWGN, toma los valores de la secuencia y los agrupa según
la cantidad de bits por símbolos correspondientes (3 bits en este caso). Toma una cantida de muestras
durante cada tiempo de símbolo para modular y grafica el resultado
- Parámetros:
sequence: Objeto de la librería Numpy con los valores de la secuencia separados de forma individual
M: Densidad de modulación
samples: Cantidad de muestras a tomar durannte el tiempo de símbolo
- Returns:
Xt: Lista con los valores modulados con ruido que simula la señal X(t)
"""
def modulation_8PAM_awgn(sequence, M, samples):
    values = list()
    for i in range(0, len(sequence), 3):
        if sequence[i] == 0 and sequence[i+1] == 0 and sequence[i+2] == 0:
            values.append(-1)  
        elif sequence[i] == 0 and sequence[i+1] == 0 and sequence[i+2] == 1:
            values.append(-0.5)  
        elif sequence[i] == 0 and sequence[i+1] == 1 and sequence[i+2] == 0:
            values.append(-0.25)  
        elif sequence[i] == 0 and sequence[i+1] == 1 and sequence[i+2] == 1:
            values.append(-0.125)  
        elif sequence[i] == 1 and sequence[i+1] == 0 and sequence[i+2] == 0:
            values.append(0.125) 
        elif sequence[i] == 1 and sequence[i+1] == 0 and sequence[i+2] == 1:
            values.append(0.25) 
        elif sequence[i] == 1 and sequence[i+1] == 1 and sequence[i+2] == 0:
            values.append(0.5) 
        elif sequence[i] == 1 and sequence[i+1] == 1 and sequence[i+2] == 1:
            values.append(1)  
    pam_values = np.array(values)
    pt = np.empty(samples)
    pt.fill(1)
    Xt_awgn = list()
    Xt_awgn_plot = list()
    """
    Las siguientes líneas crean el ruido y se agrega a los datos de la simulación para simular el canal AWGN
    """
    noise = np.random.normal(0,0.1,samples)
    """
    Este ciclo se encarga de la modulación
    """
    for j in range(len(pam_values)):
        Xi_awgn = list()
        for k in range(len(pt)):
            Xt_awgn_plot.append((pam_values[j]*pt[k]) + noise[k])
            Xi_awgn.append((pam_values[j]*pt[k]) + noise[k])
        Xt_awgn.append(Xi_awgn)
    time = np.arange(len(Xt_awgn_plot))
    title1 = "Señal (con ruido) de modulación 8-PAM para la secuencia de bits: " + sys.argv[1]
    """
    Se grafican los valores modulados
    """
    plt.figure(1)
    plt.plot(time,Xt_awgn_plot)
    plt.title(title1)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Xt_awgn

"""
- Método demodulation_4PAM():
Función que simula la demodulación 4-PAM ideal, toma los valores modulación y saca un su promedio. Luego
se compara contra el valor esperado exacto del símbolo, ya que la idealización del modelo lo permite,
para recuperar así los bits de la secuencia
- Parámetros:
Xt: Lista que contiene el valor de las amplitudes obtenidas durante la modulación
samples: Cantidad de muestra por tiempo de símbolo. En este caso se usa como Ts para iterar sobre los datos
- Returns:
Rx_sequence: String que contiene la secuencia de bits recuperada en la demodulación
"""
def demodulation_4PAM(Xt, samples):
    average = list()
    """
    Realiza el promedio de los datos modulados durante un tiempo de símbolo
    """
    for i in range(len(Xt)):
        average_val = 0
        for j in range(0, samples):
            average_val += Xt[i][j]
        average.append(average_val/samples)
    Rx_sequence = ''
    """
    Compara el valor de los promedios contra el esperado para cada símbolo
    """
    for k in range(len(average)):
        if average[k] == -1:
            Rx_sequence = Rx_sequence + "00"
        elif average[k] == -0.5:
            Rx_sequence = Rx_sequence + "01"
        elif average[k] == 0.5:
            Rx_sequence = Rx_sequence + "10"
        elif average[k] == 1:
            Rx_sequence = Rx_sequence + "11"
    pt = np.empty(samples)
    pt.fill(1)
    Xt_plot = list()
    for j in range(len(average)):
        for k in range(len(pt)):
            Xt_plot.append(average[j]*pt[k])
    time = np.arange(len(Xt_plot))
    title = "Señal de demodulación (sin ruido) 4-PAM para la secuencia recuperada de: " + Rx_sequence
    """
    Grafica los datos obtenidos luego de la demodulación
    """
    plt.figure(1)
    plt.plot(time,Xt_plot)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Rx_sequence

"""
- Método demodulation_4PAM_awgn():
Función que simula la demodulación 4-PAM con ruido, toma los valores modulación y saca un su promedio. Luego
se compara contra el valor esperado exacto del símbolo, ya que la idealización del modelo lo permite,para 
recuperar así los bits de la secuencia
- Parámetros:
Xt_awgn: Lista que contiene el valor de las amplitudes obtenidas durante la modulación
samples: Cantidad de muestra por tiempo de símbolo. En este caso se usa como Ts para iterar sobre los datos
- Returns:
Rx_sequence: String que contiene la secuencia de bits recuperada en la demodulación
"""
def demodulation_4PAM_awgn(Xt_awgn, samples):
    average = list()
    """
    Realiza el promedio de los datos modulados durante un tiempo de símbolo
    """
    for i in range(len(Xt_awgn)):
        average_val = 0
        for j in range(0, samples):
            average_val += Xt_awgn[i][j]
        average.append(average_val/samples)
    Rx_sequence_awgn = ''
    """
    Compara el valor de los promedios contra el esperado para cada símbolo
    """
    for k in range(len(average)):
        if average[k] <= -0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "00"
        elif average[k] < 0 and average[k] > -0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "01"
        elif average[k] > 0 and average[k] < 0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "10"
        elif average[k] >= 0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "11"
    pt = np.empty(samples)
    pt.fill(1)
    Xt_plot_awgn = list()
    for j in range(len(average)):
        for k in range(len(pt)):
            Xt_plot_awgn.append(average[j]*pt[k])
    time = np.arange(len(Xt_plot_awgn))
    title = "Señal de demodulación (con ruido) 4-PAM para la secuencia recuperada de: " + Rx_sequence_awgn
    """
    Grafica los datos obtenidos luego de la demodulación
    """
    plt.figure(1)
    plt.plot(time,Xt_plot_awgn)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Rx_sequence_awgn

"""
- Método demodulation_8PAM():
Función que simula la demodulación 8-PAM ideal, toma los valores modulación y saca un su promedio. Luego
se compara contra el valor esperado exacto del símbolo, ya que la idealización del modelo lo permite,
para recuperar así los bits de la secuencia
- Parámetros:
Xt: Lista que contiene el valor de las amplitudes obtenidas durante la modulación
samples: Cantidad de muestra por tiempo de símbolo. En este caso se usa como Ts para iterar sobre los datos
- Returns:
Rx_sequence: String que contiene la secuencia de bits recuperada en la demodulación
"""
def demodulation_8PAM(Xt, samples):
    average = list()
    """
    Realiza el promedio de los datos modulados durante un tiempo de símbolo
    """
    for i in range(len(Xt)):
        average_val = 0
        for j in range(0, samples):
            average_val += Xt[i][j]
        average.append(average_val/samples)
    Rx_sequence = ''
    """
    Compara el valor de los promedios contra el esperado para cada símbolo
    """
    for k in range(len(average)):
        if average[k] == -1:
            Rx_sequence = Rx_sequence + "000"
        elif average[k] == -0.5:
            Rx_sequence = Rx_sequence + "001"
        elif average[k] == -0.25:
            Rx_sequence = Rx_sequence + "010"
        elif average[k] == -0.125:
            Rx_sequence = Rx_sequence + "011"
        elif average[k] == 0.125:
            Rx_sequence = Rx_sequence + "100"
        elif average[k] == 0.25:
            Rx_sequence = Rx_sequence + "101"
        elif average[k] == 0.5:
            Rx_sequence = Rx_sequence + "110"
        if average[k] == 1:
            Rx_sequence = Rx_sequence + "111"
    pt = np.empty(samples)
    pt.fill(1)
    Xt_plot = list()
    for j in range(len(average)):
        for k in range(len(pt)):
            Xt_plot.append(average[j]*pt[k])
    time = np.arange(len(Xt_plot))
    title = "Señal de demodulación (sin ruido) 8-PAM para la secuencia recuperada de: " + Rx_sequence
    """
    Grafica los datos obtenidos luego de la demodulación
    """
    plt.figure(1)
    plt.plot(time,Xt_plot)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Rx_sequence

"""
- Método demodulation_8PAM_awgn():
Función que simula la demodulación 4-PAM con ruido, toma los valores modulación y saca un su promedio. Luego
se compara contra el valor esperado exacto del símbolo, ya que la idealización del modelo lo permite,para 
recuperar así los bits de la secuencia
- Parámetros:
Xt_awgn: Lista que contiene el valor de las amplitudes obtenidas durante la modulación
samples: Cantidad de muestra por tiempo de símbolo. En este caso se usa como Ts para iterar sobre los datos
- Returns:
Rx_sequence: String que contiene la secuencia de bits recuperada en la demodulación
"""
def demodulation_8PAM_awgn(Xt_awgn, samples):
    average = list()
    """
    Realiza el promedio de los datos modulados durante un tiempo de símbolo
    """
    for i in range(len(Xt_awgn)):
        average_val = 0
        for j in range(0, samples):
            average_val += Xt_awgn[i][j]
        average.append(average_val/samples)
    Rx_sequence_awgn = ''
    """
    Compara el valor de los promedios contra el esperado para cada símbolo
    """
    for k in range(len(average)):
        if average[k] <= -0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "000"
        elif average[k] > -0.75 and average[k] <= -0.375:
            Rx_sequence_awgn = Rx_sequence_awgn + "001"
        elif average[k] > -0.375 and average[k] <= -0.1875:
            Rx_sequence_awgn = Rx_sequence_awgn + "010"
        elif average[k] > -0.1875 and average[k] < 0:
            Rx_sequence_awgn = Rx_sequence_awgn + "011"
        elif average[k] < 0.1875 and average[k] > 0:
            Rx_sequence_awgn = Rx_sequence_awgn + "100"
        elif average[k] < 0.375 and average[k] >= 0.1875:
            Rx_sequence_awgn = Rx_sequence_awgn + "101"
        elif average[k] < 0.75 and average[k] >= 0.375:
            Rx_sequence_awgn = Rx_sequence_awgn + "110"
        if average[k] >= 0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "111"
    pt = np.empty(samples)
    pt.fill(1)
    Xt_plot_awgn = list()
    for j in range(len(average)):
        for k in range(len(pt)):
            Xt_plot_awgn.append(average[j]*pt[k])
    time = np.arange(len(Xt_plot_awgn))
    title = "Señal de demodulación (con ruido) 8-PAM para la secuencia recuperada de: " + Rx_sequence_awgn
    """
    Grafica los datos obtenidos luego de la demodulación
    """
    plt.figure(1)
    plt.plot(time,Xt_plot_awgn)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Rx_sequence_awgn

"""
- Método modulation(): 
Función que llama a la función correspondiente según la densidad de modulación
- Parámetros:
sequence: Obtejo de la librería Numpy con los valores de la secuencia de bits
separados de forma individual
M: Densidad de modulación
smaples: Cantidad de muestras por tiempo de símbolo
- Returns:
Xt: Lista con los valores modulados de forma ideal
Xt_awgn: Lista con los valores modulados con el canal AWGN
"""
def modulation(sequence, M, samples):
    if M == 4:
        Xt = modulation_4PAM(sequence, M, samples)
        Xt_awgn = modulation_4PAM_awgn(sequence, M, samples)
    elif M == 8:
        Xt = modulation_8PAM(sequence, M, samples)
        Xt_awgn = modulation_8PAM_awgn(sequence, M, samples)
    return Xt, Xt_awgn

"""
- Método demodulation():
Función que llama a la función correspondiente según la densidad de modulación
- Parámetros:
Xt: Lista con los valores modulados de forma ideal
Xt_awgn: Lista con los valores modulados con el canal AWGN
M: Densidad de modulación
smaples: Cantidad de muestras por tiempo de símbolo
- Returns:
Rx_sequence: Secuencia recuperada en la demodulación ideal
Rx_sequence_awgn: Secuencia recuperada en la demodulación con canal AWGN
"""
def demodulation(Xt, Xt_awgn, M, samples):
    if M == 4:  
        Rx_sequence = demodulation_4PAM(Xt, samples)
        Rx_sequence_awgn = demodulation_4PAM_awgn(Xt_awgn, samples)
    elif M == 8:
        Rx_sequence = demodulation_8PAM(Xt, samples)
        Rx_sequence_awgn = demodulation_8PAM_awgn(Xt_awgn, samples)
    return Rx_sequence, Rx_sequence_awgn

"""
Médodo main():
Función main del programa
"""
def main():
    M, samples = PAM_parameters()
    if samples < 20:
        print("Debe escoger al menos 20 muestras para esta simulación")
    else:
        if M == 4 or M == 8:
            sequence = readSequence(M)
            Xt, Xt_awgn = modulation(sequence, M, samples)
            Rx_sequence, Rx_sequence_awgn = demodulation(Xt, Xt_awgn, M, samples)
            print("La secuencia antes del proceso de modulación es: ", sys.argv[1])
            print("La secuencia después del proceso de demodulación (sin ruido) es: ", Rx_sequence)
            print("La secuencia después del proceso de demodulación (con ruido) es: ", Rx_sequence_awgn)
        else:
            print("No ha escogido ninguna de las dos densidades de modulación disponibles")
            print("Debe escoger 4 (4-PAM) u 8 (8-PAM) que son las modulaciones disponibles en esta simulación")
if __name__ == '__main__':
    main()
