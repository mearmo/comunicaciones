# Ingeniería de las comunicaciones: Simulación de un modulador y demodulador 4-PAM y 8-PAM

* Grupo 5G
________________________________________________
________________________________________________

# Instrucciones de uso:
A continuación se muestran los comandos que debe ejecutar según la operación que desee realizar. El 
repositorio cuenta con un archivo Makefile para correr el programa, en este puede cambiar la secuencia
de bits para la simulación, la densidad de modulación y la cantidad de muestras a realizar.
Debe tener instaladas las librerías Numpy y Matplotlib para poder ejecutar el programa.

#### Compilar el programa y ejecutar el programa (primera opción)
    $ make

#### Compilar el programa y ejecutar el programa (segunda opción)
    $ make all

    