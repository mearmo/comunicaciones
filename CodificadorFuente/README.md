**CODIFICADOR DE FUENTE**

Esta codificación de fuente fue hecha con codificación de Huffman en Python3.

Para ejecutar la codificación: *python3 huffman.py data.txt test 0.001*
O tambien: *make codificador*
Donde 0.001 o el ultimo argumento es la probabilidad de error en los bits para el BSC

Para ejecutar la decodificación: *python3 decodificador.py test resultados.txt*
O tambien: *make decodificador*

Para comparar el archivo de entrada conlos de salida:* make comparar*

Para realizar todo esto seguido: *make all*
