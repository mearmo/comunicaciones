#!/usr/bin/env python3
import sys
import codecs
import heapq
import pickle
import json
import random 
from collections import Counter

'''Para implementar la codificacion de Huffman es necesario obtener cual es la probabilidad de que 
aparezca cada simbolo. Para ello se toman los caracteres del archivo y cuantes veces se repite 
cada uno y asi calcular su probabilidad. '''

def probabilidades(data):
    total = len(data)
    #La funcion Counter cuenta cuantas veces se repite el mismo caracter y guarda en una columna 
    # de items a cada símbolo, y en otra columna la cantidad
    #Hay k cantidad de simbolos guardados en vT
    vT = Counter(data) 
    #Se ordenan las probabilidades de mayor a menor
    orden = sorted(vT.items(), key=lambda pair: pair[1], reverse=True)
    proba = {}
    for i,j in orden:
        proba[i] = float(j)/total
        #print(proba[i],j)
    proba['end'] = 1.0/total #se agrega un ultimo item de fin de linea con la propabilidad de 
    #1 entre el total para conocer cual es el fin de la entrada de bits. 
    return proba

'''Es necesario contar con un arbol binario de datos para crear los caminos 
de la secuencia asignada a cada caracter'''
def arbol(data):
    tree = []
    for i,j in data.items(): #en i de data vienen los caracteres y en j las probabilidades
        #print(i,j)
        heapq.heappush(tree,(j,0,i)) #heap es una libreria utilizada para crear arboles binarios
        #como los datos vienen ordenados de mayor a menor, se agregan todos al arbol inicial

    while len(tree) > 1:
        #se agregan los datos del arbol a dos ramas para ir sumando las probabilidades y creando el arbol de más probable a menos probable 
        rama_iz = heapq.heappop(tree)
        rama_der = heapq.heappop(tree)
        n = (rama_iz[0]+rama_der[0],max(rama_iz[1],rama_der[1])+1,[rama_iz,rama_der])
        heapq.heappush(tree,n)
    return tree[0] #devuelve la raiz inicial del arbol

''' Se crea un diccionario de simbolos para asociar cada secuencia al caracter correspondiente'''
def dicSimbolos(tree):
    vT_bkT = {}
    L = []
    L.append(tree+("",)) #la lista tiene la forma de la entrada del arrbol y el prefijo de la secuencia
    while len(L) > 0:
        bk = L.pop() #Se extrae elemento por elemento para crear la secuencia de cada caracter
        if type(bk[2]) == list:
            prefijo = bk[-1] #el prefijo va a ser el ultimo elemento de la lista
            L.append(bk[2][1]+(prefijo+"0",))
            L.append(bk[2][0]+(prefijo+"1",))
            continue
        else:
            vT_bkT[bk[2]] = bk[-1] #bk es la secuencia para un simbolo y vT_bkT es el conjunto de caracteres y secuencias
            #print(vT_bkT[bk[2]])
        pass
    return vT_bkT

'''Conversion de caracteres a secuencia de bit binarios para comprimir el archivo sin el BSC'''
def comprimir(dic,data):
    bkT = ""
    for k in data:
        bk = dic[k]
        bkT = bkT + bk
    bkT = '1' + bkT + dic['end'] # Bit 1 de inicio mas la secuencia mas el bit final
    bkT = bkT + (len(bkT) % 8 * "0") #Se necesita añadir ceros para que sea divisible entre 8 y poder crear bytes
    return int(bkT,2)

'''Cambia los 0s por 1s y viceversa'''
def switch_bits(argument):
    switcher = {
        "1": "0",
        "0": "1",
    }
    return (switcher.get(argument, "Invalid bit")) 

'''Conversion de caracteres a secuencia de bit binarios para comprimir el archivo con el BSC'''
def comprimirBSC(dic,data):
    bkT = ""
    p = float(sys.argv[3]) #probabilidad de error canal BSC
    total_bits = 0
    bit_cambio = 0
    for k in data:
        bk = dic[k]
        for j in range(len(bk)):
            l = ""
            total_bits += 1
            rand = random.uniform(0.0, 1.0) 
            #print(rand)
            if p > rand: #si un numero aleatorio es menos a la probabilidad, se intercambian los bits
                l = switch_bits(bk[j])
                bit_cambio += 1
                #print("Se cambiaron los bits")
            else:
                l = bk[j] #en caso contrario, se mantienen los bits
            bkT = bkT + l
    bkT = '1' + bkT + dic['end'] # Bit 1 de inicio mas la secuencia mas el bit final
    bkT = bkT + (len(bkT) % 8 * "0") #Se necesita añadir ceros para que sea divisible entre 8 y poder crear bytes
    porcentaje = bit_cambio/total_bits *100 #porcentaje de bits cambiados
    print("Se cambiaron " + str(bit_cambio) + " de " + str(total_bits) + " bits: " + str(porcentaje) + "%")
    return int(bkT,2)

if __name__ == "__main__":
    # Leer archivo de texto
    with codecs.open(sys.argv[1],'r','utf-8') as inf:
        vk = inf.read()
    inf.close()
    # Calcular porbabilidades de aparicion de cada caracter
    p_v = probabilidades(vk)
    # Arbol binario para la codificacion de Huffman
    arbol_binario = arbol(p_v)
    # Diccionario de simbolo y secuencia
    dic = dicSimbolos(arbol_binario)
    # Comprimir el archivo
    bkT = comprimir(dic,vk)
    bkT2 = comprimirBSC(dic,vk)

    #Guardar archivo comprimido
    outf = open(sys.argv[2],'wb')
    pickle.dump(bkT,outf) #la libreria pickle pasa de binario a bytes
    outf.close()
    #Guardar archivo comprimido
    outf = open(sys.argv[2]+"BSC",'wb')
    pickle.dump(bkT2,outf) #la libreria pickle pasa de binario a bytes
    outf.close()
    #Guardar diccionario
    outf = open(sys.argv[2]+".dic",'w')
    json.dump(dic,outf) #json se utiliza para manejar datos como bases de datos, tablas de informacion o diccionarios. 
    outf.close()