#!/usr/bin/env python3
import sys
import json
import pickle
import codecs

'''El decodificador utiliza el diccionario para convertir la secuencia de datos 
binario a la lista de acaracteres original'''
def decodificador(dic, bkT):
    vT = []
    cantidad_bits = bkT.bit_length() - 1 #se le quita el bit de fin que se agrego en el codificador
    done = False
    while cantidad_bits > 0 and not done:
        muestra = cantidad_bits - 1 #definir la cantidad de bits que se tomaran de la muestra
        while True:
            num = bkT >> muestra #se extrae una muestra del conjunto total de bits
            bk = bin(num)[3:] # quitar de la secuencia los bits de inicio y de formato
            if bk not in dic:
                muestra -= 1 # si le secuencia no está en el diccionario se prueba con un menor numero de bit por secuencia. 
                continue # y se vuelve al comienzo
            vk = dic[bk] # si esta en el diccionaro, se escribe el caracter correspondinete a esa secuencia
            if vk == 'end': # se se llega al final de la secuencia, la decodificacion esta lista
                done = True
                break
            vT.append(vk) #crear todo el conjunto de caracteres
            bkT = bkT - ((num - 1) << muestra) #se extrae la secuencia obtenida del conjunto de bits de entrada
            cantidad_bits = muestra
    return ''.join(vT)

if __name__ == "__main__":
    f = open("test.dic")
    dic = json.load(f) #cargar el diccionario creado por el codificador
    dic2 = {} #se cargan los datos en un nuevo diccionario
    for i,e in dic.items(): dic2[e] = i
    f.close()

    f = open(sys.argv[1],'rb') #cargar la secuencia de datos enviada por el codificador
    bstr = pickle.load(f) #pickle convierte de bytes a binario
    f.close()
    #decodificar la informacion
    content = decodificador(dic2,bstr)
    #crear nuevo archivo de texto con los resultados
    with codecs.open(sys.argv[2],'w','utf-8') as f:
    #f = open(sys.argv[2],'w')
        f.write(content)    
    f.close()