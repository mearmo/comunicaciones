#!/usr/bin/env python3
import sys
import codecs
import heapq
import pickle
import json
import random 
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt

'''Para implementar la codificacion de Huffman es necesario obtener cual es la probabilidad de que 
aparezca cada simbolo. Para ello se toman los caracteres del archivo y cuantes veces se repite 
cada uno y asi calcular su probabilidad. '''

def probabilidades(data):
    total = len(data)
    #La funcion Counter cuenta cuantas veces se repite el mismo caracter y guarda en una columna 
    # de items a cada símbolo, y en otra columna la cantidad
    #Hay k cantidad de simbolos guardados en vT
    vT = Counter(data) 
    #Se ordenan las probabilidades de mayor a menor
    orden = sorted(vT.items(), key=lambda pair: pair[1], reverse=True)
    proba = {}
    for i,j in orden:
        proba[i] = float(j)/total
        #print(proba[i],j)
    proba['end'] = 1.0/total #se agrega un ultimo item de fin de linea con la propabilidad de 
    #1 entre el total para conocer cual es el fin de la entrada de bits. 
    return proba

'''Es necesario contar con un arbol binario de datos para crear los caminos 
de la secuencia asignada a cada caracter'''
def arbol(data):
    tree = []
    for i,j in data.items(): #en i de data vienen los caracteres y en j las probabilidades
        #print(i,j)
        heapq.heappush(tree,(j,0,i)) #heap es una libreria utilizada para crear arboles binarios
        #como los datos vienen ordenados de mayor a menor, se agregan todos al arbol inicial

    while len(tree) > 1:
        #se agregan los datos del arbol a dos ramas para ir sumando las probabilidades y creando el arbol de más probable a menos probable 
        rama_iz = heapq.heappop(tree)
        rama_der = heapq.heappop(tree)
        n = (rama_iz[0]+rama_der[0],max(rama_iz[1],rama_der[1])+1,[rama_iz,rama_der])
        heapq.heappush(tree,n)
    return tree[0] #devuelve la raiz inicial del arbol

''' Se crea un diccionario de simbolos para asociar cada secuencia al caracter correspondiente'''
def dicSimbolos(tree):
    vT_bkT = {}
    L = []
    L.append(tree+("",)) #la lista tiene la forma de la entrada del arrbol y el prefijo de la secuencia
    while len(L) > 0:
        bk = L.pop() #Se extrae elemento por elemento para crear la secuencia de cada caracter
        if type(bk[2]) == list:
            prefijo = bk[-1] #el prefijo va a ser el ultimo elemento de la lista
            L.append(bk[2][1]+(prefijo+"0",))
            L.append(bk[2][0]+(prefijo+"1",))
            continue
        else:
            vT_bkT[bk[2]] = bk[-1] #bk es la secuencia para un simbolo y vT_bkT es el conjunto de caracteres y secuencias
            #print(vT_bkT[bk[2]])
        pass
    return vT_bkT

'''Conversion de caracteres a secuencia de bit binarios para comprimir el archivo sin el BSC'''
def comprimir(dic,data):
    bkT = ""
    for k in data:
        bk = dic[k]
        bkT = bkT + bk
    #bkT = '1' + bkT + dic['end'] # Bit 1 de inicio mas la secuencia mas el bit final
    #bkT = bkT + (len(bkT) % 8 * "0") #Se necesita añadir ceros para que sea divisible entre 8 y poder crear bytes
    return int(bkT)

'''Agrupar en diferentes m de tamaño k'''
def group_to_m(bfT, k):
    a = list(bfT)
    new_list = []
    array_list = []
    counter = 0
    # arr = np.array([])
    if (len(bfT)%k != 0):
        a.extend(['0']*(len(bfT)%k))

    for i in range(0,len(a)+1):
        if(counter<k):
            new_list.append(a[i])
            counter = counter + 1
        else:
            if(i<len(a)+1):
                array_list.append(new_list)
                new_list = []
                if(i<len(a)):
                    new_list.append(a[i])
                counter = 1
    M = np.array(array_list)

    
    # M = np.array(a)
    return M


'''Generacion de la matriz kxn y obtencion de u = mG'''
def get_U(M, k, n):
    identity = (np.identity(k, dtype=str)) 
    new_id = np.asfarray(np.where(identity == "", "0", "1"), int)
    rand = random.sample(range(1,pow(2,n-k)), k)


    M = np.asfarray(M, int)
    M = np.ndarray.astype(M, dtype = bool)


    bin_rand = []
    for i in range(0, len(rand)):
        bin_rand.append(format(rand[i], '03b'))
    bin_rand = [list(i) for i in bin_rand]
    global rand_array
    rand_array = np.array(bin_rand) #P
    G = np.concatenate((rand_array, new_id), axis = 1)
    G = np.asfarray(G, int)
    G = np.ndarray.astype(G, dtype=bool)


    U = 1*np.dot(M, G)
    return U
    

# ##########################################################################################
# CANAL BSC
'''Con la porbabilidad de error del canal BSC se obtiene el vector de error y se caclula v = u+e'''
def get_V(u, pe, n):

    error_bits = round(pe*n)
    all_errors = []
    for j in range(0, len(u)):
        e_list = [0]*n
        random_bits = random.sample(range(0,n), error_bits)
        for i in range(0,len(random_bits)):
            e_list[random_bits[i]] = 1
        all_errors.append(e_list)
    
    errors = np.array(all_errors)
    v = np.bitwise_xor(u,errors)
    return v

'''Se crea una matriz H'''
def create_H(rand_array, k):
    identity = (np.identity(k-1, dtype=str)) 
    new_id = np.asfarray(np.where(identity == "", "0", "1"), int)
    H = np.concatenate((new_id, rand_array))
    H = np.asfarray(H, int)
    H = 1*np.ndarray.astype(H, dtype=bool)
    return H

'''Obtencion del sindorome s = vH y de e*'''
def search_Syndrome(H, vH, n):
    index = len(H)+1
    error_list = []
    for i in range(0,len(vH)):
        errors = [0]*n
        for j in range(0,len(H)):
            if((vH[i] == H[j]).all()):
                index = j
                break

        if(index < len(H)):
            errors[index] = 1
        error_list.append(errors)

    return error_list

'''u* = v+e* para obtener m* '''
def get_M(u_ast, k, n, dic):
    m_list = []
    for i in u_ast:
        m_list.append(i[n-k:n])
    m_arr = np.array(m_list)
    arr = ""
    for i in m_arr:
        for j in i:
            arr = arr + str(j)
    arr = arr + dic['end']
    return int(arr,2)

'''El decodificador utiliza el diccionario para convertir la secuencia de datos 
binario a la lista de acaracteres original'''
def decodificador(dic, bkT):
    vT = []
    cantidad_bits = bkT.bit_length() - 1 #se le quita el bit de fin que se agrego en el codificador
    done = False
    while cantidad_bits > 0 and not done:
        muestra = cantidad_bits - 1 #definir la cantidad de bits que se tomaran de la muestra
        while True:
            num = bkT >> muestra #se extrae una muestra del conjunto total de bits
            bk = bin(num)[3:] # quitar de la secuencia los bits de inicio y de formato
            if bk not in dic:
                muestra -= 1 # si le secuencia no está en el diccionario se prueba con un menor numero de bit por secuencia. 
                continue # y se vuelve al comienzo
            vk = dic[bk] # si esta en el diccionaro, se escribe el caracter correspondinete a esa secuencia
            if vk == 'end': # se se llega al final de la secuencia, la decodificacion esta lista
                done = True
                break
            vT.append(vk) #crear todo el conjunto de caracteres
            bkT = bkT - ((num - 1) << muestra) #se extrae la secuencia obtenida del conjunto de bits de entrada
            cantidad_bits = muestra
    return ''.join(vT)


"""
- Método PAM_parameters(): 
Toma la densidad de modulación introducida por el usuario y la cantidad de
muestras de muestreo para convertirlas en enteros. También imprime la asignación de bits a símbolos
según la densidad de modulación escogida.
- Returns:
M: Entero con la densidad de modulación
samples: Cantidad de muestras 
"""
def PAM_parameters():
    M = int(sys.argv[5])
    samples = int(sys.argv[6])
    if M == 4:
        print("Ha escogido modulación 4-PAM")
        print("Asignación de bits a símbolos 4-PAM")
        print("              b | a")
        print("             --------")
        print("             00 | -1")
        print("             01 |-1/2")
        print("             10 | 1/2")
        print("             11 |  1")
    elif M == 8:
        print("Ha escogido modulación 8-PAM")
        print("Asignación de bits a símbolos 8-PAM")
        print("              b  |  a")
        print("             --------")
        print("             000 | -1")
        print("             001 |-1/2")
        print("             010 |-1/4")
        print("             011 |-1/8")
        print("             100 | 1/8")
        print("             101 | 1/4")
        print("             110 | 1/2")
        print("             111 |  1") 
    return M, samples

"""
- Método split():
Separa en caracteres un strong dado y los devuelve como una lista
- Parámetros:
word: String a separar en caracteres
- Returns:
list(word): Lista cuyos elementos son los caracteres del string
"""
def split(word): 
    return list(word) 

"""
- Método readSequence():
Método que toma la secuencia de bits ingresada por el usuario y genera una lista con los caracteres
de la secuencia. También revisa que la cantidad de elementos de la secuencia sea múltiplo de la 
cantidad de bits por símbolo según la densidad de modulación. En caso de que no sea así, se agregan
ceros para completar la secuencia
- Parámetros:
M: Densidad de modulación
- Returns:
sequence: Un objeto tipo array de la librería de Numpy con los valores de los bits de la secuencia
separados de forma unitaria
"""
def readSequence(M, u):
    inputSequence = u
    lst1 = split(inputSequence)
    lst2 = list()
    for i in range(len(lst1)):
        lst2.append(int(lst1[i]))
    if M == 4:
        if len(lst2) % 2 == 1:
            lst2.append(random.randint(0,1))
    elif M == 8:
        pam_flag = False
        while pam_flag == False:
            if len(lst2) % 3 != 0:
                lst2.append(random.randint(0,1))
            else:
                pam_flag = True
    sequence = np.array(lst2)
    return sequence

"""
- Método modulation_4PAM():
Función que simula la modulación 4-PAM ideal, toma los valores de la secuencia y los agrupa según
la cantidad de bits por símbolos correspondientes (2 bits en este caso). Toma una cantida de muestras
durante cada tiempo de símbolo para modular y grafica el resultado
- Parámetros:
sequence: Objeto de la librería Numpy con los valores de la secuencia separados de forma individual
M: Densidad de modulación
samples: Cantidad de muestras a tomar durannte el tiempo de símbolo
- Returns:
Xt: Lista con los valores modulados que simula la señal X(t)
"""
def modulation_4PAM(sequence, M, samples):
    values = list()
    for i in range(0, len(sequence), 2):
        if sequence[i] == 0 and sequence[i+1] == 0:
            values.append(-1)  
        elif sequence[i] == 0 and sequence[i+1] == 1:
            values.append(-0.5)
        elif sequence[i] == 1 and sequence[i+1] == 0:
            values.append(0.5)
        elif sequence[i] == 1 and sequence[i+1] == 1:
            values.append(1)
    pam_values = np.array(values)
    pt = np.empty(samples)
    pt.fill(1)
    Xt = list()
    Xt_plot = list()
    """
    Este ciclo se encarga de la modulación
    """
    for j in range(len(pam_values)):
        Xi = list()
        for k in range(len(pt)):
            Xt_plot.append(pam_values[j]*pt[k])
            Xi.append(pam_values[j]*pt[k])
        Xt.append(Xi)
    time = np.arange(len(Xt_plot))
    title = "Señal (sin ruido) de modulación 4-PAM para la secuencia de bits: " + sys.argv[1]
    """
    Se grafican los valores modulados
    """
    plt.figure(1)
    plt.plot(time,Xt_plot)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Xt

"""
- Método modulation_4PAM_awgn():
Función que simula la modulación 4-PAM con AWGN, toma los valores de la secuencia y los agrupa según
la cantidad de bits por símbolos correspondientes (2 bits en este caso). Toma una cantida de muestras
durante cada tiempo de símbolo para modular y grafica el resultado
- Parámetros:
sequence: Objeto de la librería Numpy con los valores de la secuencia separados de forma individual
M: Densidad de modulación
samples: Cantidad de muestras a tomar durannte el tiempo de símbolo
- Returns:
Xt_awgn: Lista con los valores modulados con ruido que simula la señal X(t)
"""
def modulation_4PAM_awgn(sequence, M, samples):
    values = list()
    for i in range(0, len(sequence), 2):
        if sequence[i] == 0 and sequence[i+1] == 0:
            values.append(-1)  
        elif sequence[i] == 0 and sequence[i+1] == 1:
            values.append(-0.5)
        elif sequence[i] == 1 and sequence[i+1] == 0:
            values.append(0.5)
        elif sequence[i] == 1 and sequence[i+1] == 1:
            values.append(1)
    pam_values = np.array(values)
    pt = np.empty(samples)
    pt.fill(1)
    Xt_awgn = list()
    Xt_awgn_plot = list()
    """
    Las siguientes líneas crean el ruido y se agrega a los datos de la simulación para simular el canal AWGN
    """
    noise = np.random.normal(0,0.1,samples)
    """
    Este ciclo se encarga de la modulación
    """
    for j in range(len(pam_values)):
        Xi_awgn = list()
        for k in range(len(pt)):
            Xt_awgn_plot.append((pam_values[j]*pt[k]) + noise[k])
            Xi_awgn.append((pam_values[j]*pt[k]) + noise[k])
        Xt_awgn.append(Xi_awgn)
    time = np.arange(len(Xt_awgn_plot))
    title1 = "Señal (con ruido) de modulación 4-PAM para la secuencia de bits: " + sys.argv[1]
    """
    Se grafican los valores modulados
    """
    plt.figure(1)
    plt.plot(time,Xt_awgn_plot)
    plt.title(title1)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Xt_awgn

"""
- Método modulation_8PAM():
Función que simula la modulación 8-PAM ideal, toma los valores de la secuencia y los agrupa según
la cantidad de bits por símbolos correspondientes (3 bits en este caso). Toma una cantida de muestras
durante cada tiempo de símbolo para modular y grafica el resultado
- Parámetros:
sequence: Objeto de la librería Numpy con los valores de la secuencia separados de forma individual
M: Densidad de modulación
samples: Cantidad de muestras a tomar durannte el tiempo de símbolo
- Returns:
Xt: Lista con los valores modulados con ruido que simula la señal X(t)
"""
def modulation_8PAM(sequence, M, samples):
    values = list()
    for i in range(0, len(sequence), 3):
        if sequence[i] == 0 and sequence[i+1] == 0 and sequence[i+2] == 0:
            values.append(-1)  
        elif sequence[i] == 0 and sequence[i+1] == 0 and sequence[i+2] == 1:
            values.append(-0.5)  
        elif sequence[i] == 0 and sequence[i+1] == 1 and sequence[i+2] == 0:
            values.append(-0.25)  
        elif sequence[i] == 0 and sequence[i+1] == 1 and sequence[i+2] == 1:
            values.append(-0.125)  
        elif sequence[i] == 1 and sequence[i+1] == 0 and sequence[i+2] == 0:
            values.append(0.125) 
        elif sequence[i] == 1 and sequence[i+1] == 0 and sequence[i+2] == 1:
            values.append(0.25) 
        elif sequence[i] == 1 and sequence[i+1] == 1 and sequence[i+2] == 0:
            values.append(0.5) 
        elif sequence[i] == 1 and sequence[i+1] == 1 and sequence[i+2] == 1:
            values.append(1)  
    pam_values = np.array(values)
    pt = np.empty(samples)
    pt.fill(1)
    Xt = list()
    Xt_plot = list()
    """
    Este ciclo se encarga de la modulación
    """
    for j in range(len(pam_values)):
        Xi = list()
        for k in range(len(pt)):
            Xt_plot.append(pam_values[j]*pt[k])
            Xi.append(pam_values[j]*pt[k])
        Xt.append(Xi)
    time = np.arange(len(Xt_plot))
    title = "Señal (sin ruido) de modulación 8-PAM para la secuencia de bits: " + sys.argv[1]
    """
    Se grafican los valores modulados
    """
    plt.figure(1)
    plt.plot(time,Xt_plot)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Xt

"""
- Método modulation_8PAM_awgn():
Función que simula la modulación 8-PAM con AWGN, toma los valores de la secuencia y los agrupa según
la cantidad de bits por símbolos correspondientes (3 bits en este caso). Toma una cantida de muestras
durante cada tiempo de símbolo para modular y grafica el resultado
- Parámetros:
sequence: Objeto de la librería Numpy con los valores de la secuencia separados de forma individual
M: Densidad de modulación
samples: Cantidad de muestras a tomar durannte el tiempo de símbolo
- Returns:
Xt: Lista con los valores modulados con ruido que simula la señal X(t)
"""
def modulation_8PAM_awgn(sequence, M, samples):
    values = list()
    for i in range(0, len(sequence), 3):
        if sequence[i] == 0 and sequence[i+1] == 0 and sequence[i+2] == 0:
            values.append(-1)  
        elif sequence[i] == 0 and sequence[i+1] == 0 and sequence[i+2] == 1:
            values.append(-0.5)  
        elif sequence[i] == 0 and sequence[i+1] == 1 and sequence[i+2] == 0:
            values.append(-0.25)  
        elif sequence[i] == 0 and sequence[i+1] == 1 and sequence[i+2] == 1:
            values.append(-0.125)  
        elif sequence[i] == 1 and sequence[i+1] == 0 and sequence[i+2] == 0:
            values.append(0.125) 
        elif sequence[i] == 1 and sequence[i+1] == 0 and sequence[i+2] == 1:
            values.append(0.25) 
        elif sequence[i] == 1 and sequence[i+1] == 1 and sequence[i+2] == 0:
            values.append(0.5) 
        elif sequence[i] == 1 and sequence[i+1] == 1 and sequence[i+2] == 1:
            values.append(1)  
    pam_values = np.array(values)
    pt = np.empty(samples)
    pt.fill(1)
    Xt_awgn = list()
    Xt_awgn_plot = list()
    """
    Las siguientes líneas crean el ruido y se agrega a los datos de la simulación para simular el canal AWGN
    """
    noise = np.random.normal(0,0.1,samples)
    """
    Este ciclo se encarga de la modulación
    """
    for j in range(len(pam_values)):
        Xi_awgn = list()
        for k in range(len(pt)):
            Xt_awgn_plot.append((pam_values[j]*pt[k]) + noise[k])
            Xi_awgn.append((pam_values[j]*pt[k]) + noise[k])
        Xt_awgn.append(Xi_awgn)
    time = np.arange(len(Xt_awgn_plot))
    title1 = "Señal (con ruido) de modulación 8-PAM para la secuencia de bits: " + sys.argv[1]
    """
    Se grafican los valores modulados
    """
    plt.figure(1)
    plt.plot(time,Xt_awgn_plot)
    plt.title(title1)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Xt_awgn

"""
- Método demodulation_4PAM():
Función que simula la demodulación 4-PAM ideal, toma los valores modulación y saca un su promedio. Luego
se compara contra el valor esperado exacto del símbolo, ya que la idealización del modelo lo permite,
para recuperar así los bits de la secuencia
- Parámetros:
Xt: Lista que contiene el valor de las amplitudes obtenidas durante la modulación
samples: Cantidad de muestra por tiempo de símbolo. En este caso se usa como Ts para iterar sobre los datos
- Returns:
Rx_sequence: String que contiene la secuencia de bits recuperada en la demodulación
"""
def demodulation_4PAM(Xt, samples):
    average = list()
    """
    Realiza el promedio de los datos modulados durante un tiempo de símbolo
    """
    for i in range(len(Xt)):
        average_val = 0
        for j in range(0, samples):
            average_val += Xt[i][j]
        average.append(average_val/samples)
    Rx_sequence = ''
    """
    Compara el valor de los promedios contra el esperado para cada símbolo
    """
    for k in range(len(average)):
        if average[k] == -1:
            Rx_sequence = Rx_sequence + "00"
        elif average[k] == -0.5:
            Rx_sequence = Rx_sequence + "01"
        elif average[k] == 0.5:
            Rx_sequence = Rx_sequence + "10"
        elif average[k] == 1:
            Rx_sequence = Rx_sequence + "11"
    pt = np.empty(samples)
    pt.fill(1)
    Xt_plot = list()
    for j in range(len(average)):
        for k in range(len(pt)):
            Xt_plot.append(average[j]*pt[k])
    time = np.arange(len(Xt_plot))
    title = "Señal de demodulación (sin ruido) 4-PAM para la secuencia recuperada de: " + Rx_sequence
    """
    Grafica los datos obtenidos luego de la demodulación
    """
    plt.figure(1)
    plt.plot(time,Xt_plot)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Rx_sequence

"""
- Método demodulation_4PAM_awgn():
Función que simula la demodulación 4-PAM con ruido, toma los valores modulación y saca un su promedio. Luego
se compara contra el valor esperado exacto del símbolo, ya que la idealización del modelo lo permite,para 
recuperar así los bits de la secuencia
- Parámetros:
Xt_awgn: Lista que contiene el valor de las amplitudes obtenidas durante la modulación
samples: Cantidad de muestra por tiempo de símbolo. En este caso se usa como Ts para iterar sobre los datos
- Returns:
Rx_sequence: String que contiene la secuencia de bits recuperada en la demodulación
"""
def demodulation_4PAM_awgn(Xt_awgn, samples):
    average = list()
    """
    Realiza el promedio de los datos modulados durante un tiempo de símbolo
    """
    for i in range(len(Xt_awgn)):
        average_val = 0
        for j in range(0, samples):
            average_val += Xt_awgn[i][j]
        average.append(average_val/samples)
    Rx_sequence_awgn = ''
    """
    Compara el valor de los promedios contra el esperado para cada símbolo
    """
    for k in range(len(average)):
        if average[k] <= -0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "00"
        elif average[k] < 0 and average[k] > -0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "01"
        elif average[k] > 0 and average[k] < 0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "10"
        elif average[k] >= 0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "11"
    pt = np.empty(samples)
    pt.fill(1)
    Xt_plot_awgn = list()
    for j in range(len(average)):
        for k in range(len(pt)):
            Xt_plot_awgn.append(average[j]*pt[k])
    time = np.arange(len(Xt_plot_awgn))
    title = "Señal de demodulación (con ruido) 4-PAM para la secuencia recuperada de: " + Rx_sequence_awgn
    """
    Grafica los datos obtenidos luego de la demodulación
    """
    plt.figure(1)
    plt.plot(time,Xt_plot_awgn)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Rx_sequence_awgn

"""
- Método demodulation_8PAM():
Función que simula la demodulación 8-PAM ideal, toma los valores modulación y saca un su promedio. Luego
se compara contra el valor esperado exacto del símbolo, ya que la idealización del modelo lo permite,
para recuperar así los bits de la secuencia
- Parámetros:
Xt: Lista que contiene el valor de las amplitudes obtenidas durante la modulación
samples: Cantidad de muestra por tiempo de símbolo. En este caso se usa como Ts para iterar sobre los datos
- Returns:
Rx_sequence: String que contiene la secuencia de bits recuperada en la demodulación
"""
def demodulation_8PAM(Xt, samples):
    average = list()
    """
    Realiza el promedio de los datos modulados durante un tiempo de símbolo
    """
    for i in range(len(Xt)):
        average_val = 0
        for j in range(0, samples):
            average_val += Xt[i][j]
        average.append(average_val/samples)
    Rx_sequence = ''
    """
    Compara el valor de los promedios contra el esperado para cada símbolo
    """
    for k in range(len(average)):
        if average[k] == -1:
            Rx_sequence = Rx_sequence + "000"
        elif average[k] == -0.5:
            Rx_sequence = Rx_sequence + "001"
        elif average[k] == -0.25:
            Rx_sequence = Rx_sequence + "010"
        elif average[k] == -0.125:
            Rx_sequence = Rx_sequence + "011"
        elif average[k] == 0.125:
            Rx_sequence = Rx_sequence + "100"
        elif average[k] == 0.25:
            Rx_sequence = Rx_sequence + "101"
        elif average[k] == 0.5:
            Rx_sequence = Rx_sequence + "110"
        if average[k] == 1:
            Rx_sequence = Rx_sequence + "111"
    pt = np.empty(samples)
    pt.fill(1)
    Xt_plot = list()
    for j in range(len(average)):
        for k in range(len(pt)):
            Xt_plot.append(average[j]*pt[k])
    time = np.arange(len(Xt_plot))
    title = "Señal de demodulación (sin ruido) 8-PAM para la secuencia recuperada de: " + Rx_sequence
    """
    Grafica los datos obtenidos luego de la demodulación
    """
    plt.figure(1)
    plt.plot(time,Xt_plot)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Rx_sequence

"""
- Método demodulation_8PAM_awgn():
Función que simula la demodulación 4-PAM con ruido, toma los valores modulación y saca un su promedio. Luego
se compara contra el valor esperado exacto del símbolo, ya que la idealización del modelo lo permite,para 
recuperar así los bits de la secuencia
- Parámetros:
Xt_awgn: Lista que contiene el valor de las amplitudes obtenidas durante la modulación
samples: Cantidad de muestra por tiempo de símbolo. En este caso se usa como Ts para iterar sobre los datos
- Returns:
Rx_sequence: String que contiene la secuencia de bits recuperada en la demodulación
"""
def demodulation_8PAM_awgn(Xt_awgn, samples):
    average = list()
    """
    Realiza el promedio de los datos modulados durante un tiempo de símbolo
    """
    for i in range(len(Xt_awgn)):
        average_val = 0
        for j in range(0, samples):
            average_val += Xt_awgn[i][j]
        average.append(average_val/samples)
    Rx_sequence_awgn = ''
    """
    Compara el valor de los promedios contra el esperado para cada símbolo
    """
    for k in range(len(average)):
        if average[k] <= -0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "000"
        elif average[k] > -0.75 and average[k] <= -0.375:
            Rx_sequence_awgn = Rx_sequence_awgn + "001"
        elif average[k] > -0.375 and average[k] <= -0.1875:
            Rx_sequence_awgn = Rx_sequence_awgn + "010"
        elif average[k] > -0.1875 and average[k] < 0:
            Rx_sequence_awgn = Rx_sequence_awgn + "011"
        elif average[k] < 0.1875 and average[k] > 0:
            Rx_sequence_awgn = Rx_sequence_awgn + "100"
        elif average[k] < 0.375 and average[k] >= 0.1875:
            Rx_sequence_awgn = Rx_sequence_awgn + "101"
        elif average[k] < 0.75 and average[k] >= 0.375:
            Rx_sequence_awgn = Rx_sequence_awgn + "110"
        if average[k] >= 0.75:
            Rx_sequence_awgn = Rx_sequence_awgn + "111"
    pt = np.empty(samples)
    pt.fill(1)
    Xt_plot_awgn = list()
    for j in range(len(average)):
        for k in range(len(pt)):
            Xt_plot_awgn.append(average[j]*pt[k])
    time = np.arange(len(Xt_plot_awgn))
    title = "Señal de demodulación (con ruido) 8-PAM para la secuencia recuperada de: " + Rx_sequence_awgn
    """
    Grafica los datos obtenidos luego de la demodulación
    """
    plt.figure(1)
    plt.plot(time,Xt_plot_awgn)
    plt.title(title)
    plt.xlabel("t (Ts [s])")
    plt.ylabel("X(t)")
    plt.legend("X(t)")
    plt.show()
    return Rx_sequence_awgn

"""
- Método modulation(): 
Función que llama a la función correspondiente según la densidad de modulación
- Parámetros:
sequence: Obtejo de la librería Numpy con los valores de la secuencia de bits
separados de forma individual
M: Densidad de modulación
smaples: Cantidad de muestras por tiempo de símbolo
- Returns:
Xt: Lista con los valores modulados de forma ideal
Xt_awgn: Lista con los valores modulados con el canal AWGN
"""
def modulation(sequence, M, samples):
    if M == 4:
        Xt = modulation_4PAM(sequence, M, samples)
        Xt_awgn = modulation_4PAM_awgn(sequence, M, samples)
    elif M == 8:
        Xt = modulation_8PAM(sequence, M, samples)
        Xt_awgn = modulation_8PAM_awgn(sequence, M, samples)
    return Xt, Xt_awgn

"""
- Método demodulation():
Función que llama a la función correspondiente según la densidad de modulación
- Parámetros:
Xt: Lista con los valores modulados de forma ideal
Xt_awgn: Lista con los valores modulados con el canal AWGN
M: Densidad de modulación
smaples: Cantidad de muestras por tiempo de símbolo
- Returns:
Rx_sequence: Secuencia recuperada en la demodulación ideal
Rx_sequence_awgn: Secuencia recuperada en la demodulación con canal AWGN
"""
def demodulation(Xt, Xt_awgn, M, samples):
    if M == 4:  
        Rx_sequence = demodulation_4PAM(Xt, samples)
        Rx_sequence_awgn = demodulation_4PAM_awgn(Xt_awgn, samples)
    elif M == 8:
        Rx_sequence = demodulation_8PAM(Xt, samples)
        Rx_sequence_awgn = demodulation_8PAM_awgn(Xt_awgn, samples)
    return Rx_sequence, Rx_sequence_awgn

if __name__ == "__main__":
    # Leer archivo de texto
    with codecs.open(sys.argv[1],'r','utf-8') as inf:
        vk = inf.read()
    inf.close()
    # Calcular porbabilidades de aparicion de cada caracter
    p_v = probabilidades(vk)
    # Arbol binario para la codificacion de Huffman
    arbol_binario = arbol(p_v)
    # Diccionario de simbolo y secuencia
    dic = dicSimbolos(arbol_binario)
    # Comprimir el archivo
    bkT = comprimir(dic,vk)
    strBKT = str(bkT)

    #Guardar diccionario
    outf = open(sys.argv[2]+".dic",'w')
    json.dump(dic,outf) #json se utiliza para manejar datos como bases de datos, tablas de informacion o diccionarios. 
    outf.close()

    # M y u
    m = group_to_m(strBKT, 4)
    u = get_U(m, 4, 7)
    print(u)
    print(type(u))
    bitsarray = ""
    for i in u:
        for j in i:
            bitsarray = bitsarray + str(j)


    M, samples = PAM_parameters()
    if samples < 20:
        print("Debe escoger al menos 20 muestras para esta simulación")
    else:
        if M == 4 or M == 8:
            sequence = readSequence(M, bitsarray)
            Xt, Xt_awgn = modulation(sequence, M, samples)
            Rx_sequence, Rx_sequence_awgn = demodulation(Xt, Xt_awgn, M, samples)
            print("La secuencia antes del proceso de modulación es: ", sys.argv[1])
            print("La secuencia después del proceso de demodulación (sin ruido) es: ", Rx_sequence)
            print("La secuencia después del proceso de demodulación (con ruido) es: ", Rx_sequence_awgn)
        else:
            print("No ha escogido ninguna de las dos densidades de modulación disponibles")
            print("Debe escoger 4 (4-PAM) u 8 (8-PAM) que son las modulaciones disponibles en esta simulación")

    p = float(sys.argv[3])
    v = get_V(u, p, 7)
    v = np.ndarray.astype(v, dtype=bool)
    H = create_H(rand_array, 4)
    H = np.ndarray.astype(H, dtype=bool)
    vH = 1*np.dot(v,H) #Sindrome
    error_vectors = search_Syndrome(H, vH, 7)
    u_ast = np.bitwise_xor(v, error_vectors)
    m_ast = get_M(u_ast, 4, 7, dic)

    #Guardar archivo comprimido
    outf = open(sys.argv[2],'wb')
    pickle.dump(m_ast,outf) #la libreria pickle pasa de binario a bytes
    outf.close()

    f = open("test.dic")
    dic = json.load(f) #cargar el diccionario creado por el codificador
    dic2 = {} #se cargan los datos en un nuevo diccionario
    for i,e in dic.items(): dic2[e] = i
    f.close()

    f = open(sys.argv[2],'rb') #cargar la secuencia de datos enviada por el codificador
    bstr = pickle.load(f) #pickle convierte de bytes a binario
    f.close()
    #decodificar la informacion
    content = decodificador(dic2,bstr)
    #crear nuevo archivo de texto con los resultados
    with codecs.open(sys.argv[4],'w','utf-8') as f:
    #f = open(sys.argv[2],'w')
        f.write(content)    
    f.close()


"""
Médodo main():
Función main del programa

def main():
    M, samples = PAM_parameters()
    if samples < 20:
        print("Debe escoger al menos 20 muestras para esta simulación")
    else:
        if M == 4 or M == 8:
            sequence = readSequence(M)
            Xt, Xt_awgn = modulation(sequence, M, samples)
            Rx_sequence, Rx_sequence_awgn = demodulation(Xt, Xt_awgn, M, samples)
            print("La secuencia antes del proceso de modulación es: ", sys.argv[1])
            print("La secuencia después del proceso de demodulación (sin ruido) es: ", Rx_sequence)
            print("La secuencia después del proceso de demodulación (con ruido) es: ", Rx_sequence_awgn)
        else:
            print("No ha escogido ninguna de las dos densidades de modulación disponibles")
            print("Debe escoger 4 (4-PAM) u 8 (8-PAM) que son las modulaciones disponibles en esta simulación")
if __name__ == '__main__':
    main()
"""