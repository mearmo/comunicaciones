**SISTEMA DE COMUNICACIONES**

Sistema de comunicaciones de la union de la codificación de Huffman para el codificador de fuente, y Hamming para el codificador de canal en Python3.

Para ejecutar la codificación: *python3 Sistema.py data.txt test 0.00001 resultados.txt 4 100*
O tambien: *make sistema*

Donde data.text es el archivo de entrada y resultados.txt el de salida. El 0.00001 es la probabilidad de error, 4 u 8 el moduladr PAM y 100 la cantidad de muestras.

Para comparar el archivo de entrada conlos de salida:* make comparar*

Para realizar todo esto seguido: *make all*
