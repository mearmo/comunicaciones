#!/usr/bin/env python3
import sys
import codecs
import heapq
import pickle
import json
import random 
from collections import Counter
import numpy as np


# Esta parte obtiene la matriz u
# CODIFICACION DE CANAL
##############################################################################################
 
rand_array = np.array([])

'''Agrupar en diferentes m de tamaño k'''
def group_to_m(bfT, k):
    print("Bits de entrada:")
    print(bfT)
    a = list(bfT)
    new_list = []
    array_list = []
    counter = 0
    # arr = np.array([])
    if (len(bfT)%k != 0):
        a.extend(['0']*(len(bfT)%k))

    for i in range(0,len(a)+1):
        if(counter<k):
            new_list.append(a[i])
            counter = counter + 1
        else:
            if(i<len(a)+1):
                array_list.append(new_list)
                new_list = []
                if(i<len(a)):
                    new_list.append(a[i])
                counter = 1
    M = np.array(array_list)

    
    # M = np.array(a)
    return M

'''Generacion de la matriz kxn y obtencion de u = mG'''
def get_U(M, k, n):
    identity = (np.identity(k, dtype=str)) 
    new_id = np.asfarray(np.where(identity == "", "0", "1"), int)
    rand = random.sample(range(1,pow(2,n-k)), k)


    M = np.asfarray(M, int)
    M = np.ndarray.astype(M, dtype = bool)


    bin_rand = []
    for i in range(0, len(rand)):
        bin_rand.append(format(rand[i], '03b'))
    bin_rand = [list(i) for i in bin_rand]
    global rand_array
    rand_array = np.array(bin_rand) #P
    G = np.concatenate((rand_array, new_id), axis = 1)
    G = np.asfarray(G, int)
    G = np.ndarray.astype(G, dtype=bool)


    U = 1*np.dot(M, G)
    return U
    

# ##########################################################################################
# CANAL BSC
'''Con la porbabilidad de error del canal BSC se obtiene el vector de error y se caclula v = u+e'''
def get_V(u, pe, n):

    error_bits = round(pe*n)
    all_errors = []
    for j in range(0, len(u)):
        e_list = [0]*n
        random_bits = random.sample(range(0,n), error_bits)
        for i in range(0,len(random_bits)):
            e_list[random_bits[i]] = 1
        all_errors.append(e_list)
    
    errors = np.array(all_errors)
    v = np.bitwise_xor(u,errors)
    return v

'''Se crea una matriz H'''
def create_H(rand_array, k):
    identity = (np.identity(k-1, dtype=str)) 
    new_id = np.asfarray(np.where(identity == "", "0", "1"), int)
    H = np.concatenate((new_id, rand_array))
    H = np.asfarray(H, int)
    H = 1*np.ndarray.astype(H, dtype=bool)
    return H

'''Obtencion del sindorome s = vH y de e*'''
def search_Syndrome(H, vH, n):
    index = len(H)+1
    error_list = []
    for i in range(0,len(vH)):
        errors = [0]*n
        for j in range(0,len(H)):
            if((vH[i] == H[j]).all()):
                index = j
                break

        if(index < len(H)):
            errors[index] = 1
        error_list.append(errors)

    return error_list

'''u* = v+e* para obtener m* '''
def get_M(u_ast, k, n):
    m_list = []
    for i in u_ast:
        m_list.append(i[n-k:n])
    m_arr = np.array(m_list)
    return m_arr



m = group_to_m('010111001100', 4)

print("####################################################################")
print("U")
u = get_U(m, 4, 7)
print(u)

print("####################################################################")
print("V")
p = float(sys.argv[1])
v = get_V(u, p, 7)
print(v)
v = np.ndarray.astype(v, dtype=bool)

print("####################################################################")
print("H")
H = create_H(rand_array, 4)
print(H)
H = np.ndarray.astype(H, dtype=bool)
print("####################################################################")
print("S")
vH = 1*np.dot(v,H) #Sindrome
print(vH)
print("####################################################################")

print("E*")
error_vectors = search_Syndrome(H, vH, 7)
print(error_vectors)

print("####################################################################")
print("U*")
u_ast = np.bitwise_xor(v, error_vectors)
print(u_ast)

print("####################################################################")
print("M*")
m_ast = get_M(u_ast, 4, 7)
print(m_ast)