**SISTEMA DE COMUNICACIONES**

Sistema de comunicaciones de la union de la codificación de Huffman para el codificador de fuente, y Hamming para el codificador de canal en Python3.

Para ejecutar la codificación: *python3 Sistema.py data.txt test 0.00001 resultados.txt*
O tambien: *make sistema*
Donde 0.001 o el ultimo argumento es la probabilidad de error en los bits para el BSC

Para comparar el archivo de entrada conlos de salida:* make comparar*

Para realizar todo esto seguido: *make all*
