#!/usr/bin/env python3
import sys
import codecs
import heapq
import pickle
import json
import random 
from collections import Counter
import numpy as np

'''Para implementar la codificacion de Huffman es necesario obtener cual es la probabilidad de que 
aparezca cada simbolo. Para ello se toman los caracteres del archivo y cuantes veces se repite 
cada uno y asi calcular su probabilidad. '''

def probabilidades(data):
    total = len(data)
    #La funcion Counter cuenta cuantas veces se repite el mismo caracter y guarda en una columna 
    # de items a cada símbolo, y en otra columna la cantidad
    #Hay k cantidad de simbolos guardados en vT
    vT = Counter(data) 
    #Se ordenan las probabilidades de mayor a menor
    orden = sorted(vT.items(), key=lambda pair: pair[1], reverse=True)
    proba = {}
    for i,j in orden:
        proba[i] = float(j)/total
        #print(proba[i],j)
    proba['end'] = 1.0/total #se agrega un ultimo item de fin de linea con la propabilidad de 
    #1 entre el total para conocer cual es el fin de la entrada de bits. 
    return proba

'''Es necesario contar con un arbol binario de datos para crear los caminos 
de la secuencia asignada a cada caracter'''
def arbol(data):
    tree = []
    for i,j in data.items(): #en i de data vienen los caracteres y en j las probabilidades
        #print(i,j)
        heapq.heappush(tree,(j,0,i)) #heap es una libreria utilizada para crear arboles binarios
        #como los datos vienen ordenados de mayor a menor, se agregan todos al arbol inicial

    while len(tree) > 1:
        #se agregan los datos del arbol a dos ramas para ir sumando las probabilidades y creando el arbol de más probable a menos probable 
        rama_iz = heapq.heappop(tree)
        rama_der = heapq.heappop(tree)
        n = (rama_iz[0]+rama_der[0],max(rama_iz[1],rama_der[1])+1,[rama_iz,rama_der])
        heapq.heappush(tree,n)
    return tree[0] #devuelve la raiz inicial del arbol

''' Se crea un diccionario de simbolos para asociar cada secuencia al caracter correspondiente'''
def dicSimbolos(tree):
    vT_bkT = {}
    L = []
    L.append(tree+("",)) #la lista tiene la forma de la entrada del arrbol y el prefijo de la secuencia
    while len(L) > 0:
        bk = L.pop() #Se extrae elemento por elemento para crear la secuencia de cada caracter
        if type(bk[2]) == list:
            prefijo = bk[-1] #el prefijo va a ser el ultimo elemento de la lista
            L.append(bk[2][1]+(prefijo+"0",))
            L.append(bk[2][0]+(prefijo+"1",))
            continue
        else:
            vT_bkT[bk[2]] = bk[-1] #bk es la secuencia para un simbolo y vT_bkT es el conjunto de caracteres y secuencias
            #print(vT_bkT[bk[2]])
        pass
    return vT_bkT

'''Conversion de caracteres a secuencia de bit binarios para comprimir el archivo sin el BSC'''
def comprimir(dic,data):
    bkT = ""
    for k in data:
        bk = dic[k]
        bkT = bkT + bk
    #bkT = '1' + bkT + dic['end'] # Bit 1 de inicio mas la secuencia mas el bit final
    #bkT = bkT + (len(bkT) % 8 * "0") #Se necesita añadir ceros para que sea divisible entre 8 y poder crear bytes
    return int(bkT)

'''Agrupar en diferentes m de tamaño k'''
def group_to_m(bfT, k):
    a = list(bfT)
    new_list = []
    array_list = []
    counter = 0
    # arr = np.array([])
    if (len(bfT)%k != 0):
        a.extend(['0']*(len(bfT)%k))

    for i in range(0,len(a)+1):
        if(counter<k):
            new_list.append(a[i])
            counter = counter + 1
        else:
            if(i<len(a)+1):
                array_list.append(new_list)
                new_list = []
                if(i<len(a)):
                    new_list.append(a[i])
                counter = 1
    M = np.array(array_list)

    
    # M = np.array(a)
    return M


'''Generacion de la matriz kxn y obtencion de u = mG'''
def get_U(M, k, n):
    identity = (np.identity(k, dtype=str)) 
    new_id = np.asfarray(np.where(identity == "", "0", "1"), int)
    rand = random.sample(range(1,pow(2,n-k)), k)


    M = np.asfarray(M, int)
    M = np.ndarray.astype(M, dtype = bool)


    bin_rand = []
    for i in range(0, len(rand)):
        bin_rand.append(format(rand[i], '03b'))
    bin_rand = [list(i) for i in bin_rand]
    global rand_array
    rand_array = np.array(bin_rand) #P
    G = np.concatenate((rand_array, new_id), axis = 1)
    G = np.asfarray(G, int)
    G = np.ndarray.astype(G, dtype=bool)


    U = 1*np.dot(M, G)
    return U
    

# ##########################################################################################
# CANAL BSC
'''Con la porbabilidad de error del canal BSC se obtiene el vector de error y se caclula v = u+e'''
def get_V(u, pe, n):

    error_bits = round(pe*n)
    all_errors = []
    for j in range(0, len(u)):
        e_list = [0]*n
        random_bits = random.sample(range(0,n), error_bits)
        for i in range(0,len(random_bits)):
            e_list[random_bits[i]] = 1
        all_errors.append(e_list)
    
    errors = np.array(all_errors)
    v = np.bitwise_xor(u,errors)
    return v

'''Se crea una matriz H'''
def create_H(rand_array, k):
    identity = (np.identity(k-1, dtype=str)) 
    new_id = np.asfarray(np.where(identity == "", "0", "1"), int)
    H = np.concatenate((new_id, rand_array))
    H = np.asfarray(H, int)
    H = 1*np.ndarray.astype(H, dtype=bool)
    return H

'''Obtencion del sindorome s = vH y de e*'''
def search_Syndrome(H, vH, n):
    index = len(H)+1
    error_list = []
    for i in range(0,len(vH)):
        errors = [0]*n
        for j in range(0,len(H)):
            if((vH[i] == H[j]).all()):
                index = j
                break

        if(index < len(H)):
            errors[index] = 1
        error_list.append(errors)

    return error_list

'''u* = v+e* para obtener m* '''
def get_M(u_ast, k, n, dic):
    m_list = []
    for i in u_ast:
        m_list.append(i[n-k:n])
    m_arr = np.array(m_list)
    arr = ""
    for i in m_arr:
        for j in i:
            arr = arr + str(j)
    arr = arr + dic['end']
    return int(arr,2)

'''El decodificador utiliza el diccionario para convertir la secuencia de datos 
binario a la lista de acaracteres original'''
def decodificador(dic, bkT):
    vT = []
    cantidad_bits = bkT.bit_length() - 1 #se le quita el bit de fin que se agrego en el codificador
    done = False
    while cantidad_bits > 0 and not done:
        muestra = cantidad_bits - 1 #definir la cantidad de bits que se tomaran de la muestra
        while True:
            num = bkT >> muestra #se extrae una muestra del conjunto total de bits
            bk = bin(num)[3:] # quitar de la secuencia los bits de inicio y de formato
            if bk not in dic:
                muestra -= 1 # si le secuencia no está en el diccionario se prueba con un menor numero de bit por secuencia. 
                continue # y se vuelve al comienzo
            vk = dic[bk] # si esta en el diccionaro, se escribe el caracter correspondinete a esa secuencia
            if vk == 'end': # se se llega al final de la secuencia, la decodificacion esta lista
                done = True
                break
            vT.append(vk) #crear todo el conjunto de caracteres
            bkT = bkT - ((num - 1) << muestra) #se extrae la secuencia obtenida del conjunto de bits de entrada
            cantidad_bits = muestra
    return ''.join(vT)


if __name__ == "__main__":
    # Leer archivo de texto
    with codecs.open(sys.argv[1],'r','utf-8') as inf:
        vk = inf.read()
    inf.close()
    # Calcular porbabilidades de aparicion de cada caracter
    p_v = probabilidades(vk)
    # Arbol binario para la codificacion de Huffman
    arbol_binario = arbol(p_v)
    # Diccionario de simbolo y secuencia
    dic = dicSimbolos(arbol_binario)
    # Comprimir el archivo
    bkT = comprimir(dic,vk)
    strBKT = str(bkT)

    #Guardar diccionario
    outf = open(sys.argv[2]+".dic",'w')
    json.dump(dic,outf) #json se utiliza para manejar datos como bases de datos, tablas de informacion o diccionarios. 
    outf.close()

    m = group_to_m(strBKT, 4)
    u = get_U(m, 4, 7)
    p = float(sys.argv[3])
    v = get_V(u, p, 7)
    v = np.ndarray.astype(v, dtype=bool)
    H = create_H(rand_array, 4)
    H = np.ndarray.astype(H, dtype=bool)
    vH = 1*np.dot(v,H) #Sindrome
    error_vectors = search_Syndrome(H, vH, 7)
    u_ast = np.bitwise_xor(v, error_vectors)
    m_ast = get_M(u_ast, 4, 7, dic)

    #Guardar archivo comprimido
    outf = open(sys.argv[2],'wb')
    pickle.dump(m_ast,outf) #la libreria pickle pasa de binario a bytes
    outf.close()

    f = open("test.dic")
    dic = json.load(f) #cargar el diccionario creado por el codificador
    dic2 = {} #se cargan los datos en un nuevo diccionario
    for i,e in dic.items(): dic2[e] = i
    f.close()

    f = open(sys.argv[2],'rb') #cargar la secuencia de datos enviada por el codificador
    bstr = pickle.load(f) #pickle convierte de bytes a binario
    f.close()
    #decodificar la informacion
    content = decodificador(dic2,bstr)
    #crear nuevo archivo de texto con los resultados
    with codecs.open(sys.argv[4],'w','utf-8') as f:
    #f = open(sys.argv[2],'w')
        f.write(content)    
    f.close()